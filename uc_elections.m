clear; close all;

set(0, 'defaultFigureUnits', 'normalized');
set(0, 'defaultFigurePosition', [0.2 0.2 0.6 0.6]);
set(0, 'DefaultAxesFontSize',14);

votes = flipud([
	% gerechtigd, gestemd, blanco
	12482	2717	187	% 2022
	11919	3387	154	% 2021
	11116	3448	162	% 2020
	10677	3500	244	% 2019
	10080	3665	281	% 2018
	9415	3906	164	% 2017
	9122	3154	159	% 2016
	9402	3201	128	% 2015
	9071	2481	134	% 2014
	9241	2038	0	% 2013, geen blanco's?
	9234	2058	117	% 2012
	9603	2401	132	% 2011
	9373	3179	0	% 2010
]);

years = (2010:2022)';

f(1) = figure; hold on;
abs_turnout = votes(:, 2);
turnout = votes(:, 2)./votes(:, 1);
non_blank_turnout = (votes(:, 2) - votes(:, 3))./votes(:, 1);
bar(years, turnout, DisplayName='Turnout')

xl = xlim();
plot(xl, [1 1].*mean(turnout), 'k-', DisplayName='Mean turnout')
plot([2016.5 xl(2)], [1 1].*mean(turnout(years > 2016)), 'k--', DisplayName='Mean turnout post 2016')
relevant_events();


g = @(c, x) c(1).*(x-2017) + c(2);
[C, r] = lsqcurvefit(g, [-.5 .41], years(years > 2016), turnout(years > 2016));
G = @(x) g(C, x);
fplot(G, [2016.5 2022.5], DisplayName=sprintf('Linear downwards trend'), Color='c');

title({'Turnout UT UC Elections'; 'Raw data and averages'})
xlabel('election year')
ylabel('Fraction of people who voted')
l = legend(Location='southoutside');
l.NumColumns = 3;
print(f(1), 'turnout.svg', '-dsvg');

f(2) = figure; hold on;
lmdl_years_turnout = fitlm(years(years > 2016), turnout(years > 2016))
plot(lmdl_years_turnout);
title({'Turnout UT UC Elections'; 'Linear Regression on years > 2016'});
xlabel('election year')
ylabel('Fraction of people who voted')
print(f(2),'linear_fit_year_turnout.svg', '-dsvg')

f(3) = figure; hold on;
lmdl_votes_turnout = fitlm(votes(years > 2016, 1), turnout(years > 2016))
plot(lmdl_votes_turnout);
title('Total elegible voters as a predictor of the turnout post 2016');
xlabel('Elegible Voters')
ylabel('Turnout')
print(f(3), 'linear_fit_votes_turnout.svg', '-dsvg')

[~, idx] = min(turnout);
year_with_lowest_turnout = years(idx)

f(4) = figure; hold on;
bar(years, [votes(:, 2)'; votes(:, 1)']);
plot(2015.5.*[1 1], ylim(), 'b-', DisplayName='Leenstelsel')
plot(2019.5.*[1 1], ylim(), 'r-', DisplayName='COVID')
title({'Absolute Turnout and voters for UT UC Elections'});
xlabel('election year')
ylabel('number of (Elegible) voters')
legend({'Actual Votes', 'Elegible Voters'}, Location='northwest')
relevant_events();

print(f(4), 'absolute_turnout.svg', '-dsvg')
%%
f(5) = figure; hold on;
bar(years(2:end), diff(turnout), DisplayName='Relative Difference');
relevant_events();
legend();
xlabel('election year')
ylabel('');

print(f(5), 'difference_in_turnout.svg','-dsvg');

figure; hold on;
bar(years(2:end), diff(votes(:, 2)), DisplayName='Absolute Difference');
relevant_events();
legend()
xlabel('election year')
print 'difference_in_absolute_turnout.svg' -dsvg

f(6) = figure; hold on;
bar(years, votes(:,3)./votes(:, 1))
title('Fraction of blank votes')
xlabel('year')

print(f(6), 'blanc_votes.svg', '-dsvg');

%%
disp('Absolute Turnout Rank')
sort_turnout(abs_turnout, years);
disp('Relative Turnout Rank')
sort_turnout(turnout, years);

function sort_turnout(t, y)
	[turnout, I] = sort(t);
	place = (1:length(t))';
	year = y(I);
	disp(table(place, year, turnout));
end

function relevant_events()
	plot(2015.5.*[1 1], ylim(), 'b-', DisplayName='Leenstelsel')
	plot(2019.5.*[1 1], ylim(), 'r-', DisplayName='COVID')
end
