# UT UC Elections analysis

## Turnout
![](turnout.svg)

## Elegible voters as a predictor of turnout

To the raw election turnout data post 2016, a linear regression was performed.

![](linear_fit_votes_turnout.svg)

Linear regression model:
    y ~ 1 + x1

Estimated Coefficients:
|            |   Estimate   |      SE      |  tStat  |    pValue    |
|:---------- | ------------:| ------------:| -------:| ------------:|
|(Intercept) |      0.95762 |    0.058696  |  16.315 |  8.2606e-05  |
|x1          |  -5.8266e-05 |  5.3372e-06  | -10.917 |  0.00039979  |


Number of observations: 6, Error degrees of freedom: 4
Root Mean Squared Error: 0.0136
R-squared: 0.968,  Adjusted R-Squared: 0.959
F-statistic vs. constant model: 119, p-value = 0.0004

This regression shows that post 2016 the turnout is highly correlated with the number of
eligible voters.

## Absolute Turnout
![](absolute_turnout.svg)

As is evident in this graph, post 2016 the number of eligible voters steadily increases
post 2016, while the number of votes cast slowly shrinks. This points to the council
failing to connect with this new generation of students.

## Difference in Turnout compared to last election
![](difference_in_turnout.svg)

## Difference in absolute turnout
![](difference_in_absolute_turnout.svg)

In this bar graph, the aftershocks of COVID as asserted by the council can be seen. But
also recall the [absolute turnout](#absolute-turnout) — the increase in students this year is
already larger than the decrease in voters. Moreover, it is also evident that the drop in
votes isn't very unusual in the turnout's history — especially considering the
[difference in relative turnout](#difference-in-turnout-compared-to-last-election).

## Absolute Turnout Rank

| place |  year  | turnout |
| :---: | :----: | :-----: |
|   1   |  2013  |  2038   |
|   2   |  2012  |  2058   |
|   3   |  2011  |  2401   |
|   4   |  2014  |  2481   |
|   5   |  2022  |  2717   |
|   6   |  2016  |  3154   |
|   7   |  2010  |  3179   |
|   8   |  2015  |  3201   |
|   9   |  2021  |  3387   |
|  10   |  2020  |  3448   |
|  11   |  2019  |  3500   |
|  12   |  2018  |  3665   |
|  13   |  2017  |  3906   |

## Relative Turnout Rank
| place |  year  | turnout |
| :---: | :----: | :-----: |
|   1   |  2022  | 0.21767 |
|   2   |  2013  | 0.22054 |
|   3   |  2012  | 0.22287 |
|   4   |  2011  | 0.25003 |
|   5   |  2014  | 0.27351 |
|   6   |  2021  | 0.28417 |
|   7   |  2020  | 0.31018 |
|   8   |  2019  | 0.32781 |
|   9   |  2010  | 0.33917 |
|  10   |  2015  | 0.34046 |
|  11   |  2016  | 0.34576 |
|  12   |  2018  | 0.36359 |
|  13   |  2017  | 0.41487 |
